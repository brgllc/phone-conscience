package com.brg.phoneconscience;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;
import android.widget.Toast;

import com.brg.phoneconscience.database.Song;
import com.brg.phoneconscience.database.SongsDataSource;
import com.brg.phoneconscience.helpers.NotificationHelper;
import com.brg.phoneconscience.helpers.PreferencesHelper;

import java.util.ArrayList;

public class PhoneConscienceService extends Service implements DropDetector.DropDetectorListener {

    private static final String TAG         = "PhoneConscienceService";
    public  static final String CLIENT      = "com.brg.phoneconscience.CLIENT";
    public  static final String KEEP_CLIENT = "com.brg.phoneconscience.KEEP_CLIENT";
    public  static final String STOP_PHONECONSCIENCE_SERVICE = "com.brg.phoneconscience.STOP_PHONECONSCIENCE_SERVICE";
    public  static final String RESET_NOTIFICATION = "com.brg.phoneconscience.RESET_NOTIFICATION";

    /** Keeps track of all current registered clients. */
    ArrayList<Messenger> mClients = new ArrayList<Messenger>();

    /**
     * Command to the service to register a client, receiving callbacks
     * from the service.  The Message's replyTo field must be a Messenger of
     * the client where callbacks should be sent.
     */
    static final int MSG_REGISTER_CLIENT = 1;

    /**
     * Command to the service to unregister a client, ot stop receiving callbacks
     * from the service.  The Message's replyTo field must be a Messenger of
     * the client as previously given with MSG_REGISTER_CLIENT.
     */
    static final int MSG_UNREGISTER_CLIENT = 2;

    /** Command to the service to display a message */
    static final int MSG_START_DROP_DETECTION = 50;

    static final int MSG_DROP_DETECTED = 100;

    private PhoneConscienceReceiver mReceiver = new PhoneConscienceReceiver();

    @Override
    public void dropDetected() {
        Log.i(TAG, "dropDetected");
        for (int i = 0; i < mClients.size(); i++) {
            try {
                mClients.get(i).send(Message.obtain(null, MSG_DROP_DETECTED, 0, 0));
            } catch (RemoteException e) {
                // The client is dead.  Remove it from the list;
                // we are going through the list from back to front
                // so this is safe to do inside the loop.
                mClients.remove(i);
            }
        }

        NotificationHelper.sendDropNotification(this);
    }

    @Override
    public SharedPreferences getSharedPreferences() {
        return getSharedPreferences("PhoneConscience", MODE_PRIVATE);
    }

    /**
     * Handler of incoming messages from clients.
     */
    class IncomingHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MSG_REGISTER_CLIENT:
                    mClients.add(msg.replyTo);
                    break;
                case MSG_UNREGISTER_CLIENT:
                    mClients.remove(msg.replyTo);
                    break;
                case MSG_START_DROP_DETECTION:
                    startDropDetection();
                    break;
                default:
                    super.handleMessage(msg);
            }
        }
    }
    /**
     * Target we publish for clients to send messages to IncomingHandler.
     */
    final Messenger mMessenger = new Messenger(new IncomingHandler());

    /**
     * When binding to the service, we return an interface to our messenger
     * for sending messages to the service.
     */
    @Override
    public IBinder onBind(Intent intent) {
        Toast.makeText(getApplicationContext(), "Binding to Service", Toast.LENGTH_SHORT).show();
        return mMessenger.getBinder();
    }

    private DropDetector    mDropDetector;
    private SensorManager   mSensorManager;

    private SongsDataSource datasource;

    public PhoneConscienceService() {}

    public class PhoneConscienceBinder extends Binder {
        PhoneConscienceService getService() {
            return PhoneConscienceService.this;
        }
    }

    @Override
    public  void onCreate() {
        Log.i(TAG, "[SERVICE] onCreate");
        super.onCreate();

        datasource = new SongsDataSource(this);
        datasource.open();

        IntentFilter receivedActions = new IntentFilter();
        receivedActions.addAction(Intent.ACTION_SCREEN_OFF);
        receivedActions.addAction(Intent.ACTION_SCREEN_ON);
        receivedActions.addAction("com.android.music.metachanged");
        receivedActions.addAction("com.android.music.playstatechanged");
        receivedActions.addAction("com.android.music.playbackcomplete");
        receivedActions.addAction("com.android.music.queuechanged");
        registerReceiver(mReceiver, receivedActions);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i(TAG, "[SERVICE] onStartCommand");

        if (mDropDetector == null) {
            // Start detecting
            registerDetector();
        }

        if (intent.hasExtra(CLIENT) && intent.hasExtra(KEEP_CLIENT)) {
            if (intent.getBooleanExtra(KEEP_CLIENT, true)) {
                mClients.add((Messenger) intent.getParcelableExtra(CLIENT));
            } else {
                mClients.remove(intent.getParcelableExtra(CLIENT));
            }
        }

        NotificationHelper.startPersistent(this);

        return Service.START_STICKY;
    }

    @Override
    public void onDestroy() {
        Log.i(TAG, "[SERVICE] onDestroy");
        // Cancel the persistent notification.
        NotificationHelper.cancelNotifications(this);

        unregisterReceiver(mReceiver);
        unregisterDetector();

        datasource.close();
        super.onDestroy();
    }

    private class PhoneConscienceReceiver extends BroadcastReceiver {

        public boolean screenOff;

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            if (action.equals(Intent.ACTION_SCREEN_OFF)) {
                screenOff = true;
                PreferencesHelper.setScreenOffTime(context);
                PhoneConscienceService.this.unregisterDetector();
                PhoneConscienceService.this.registerDetector();
            } else if (intent.getAction().equals(Intent.ACTION_SCREEN_ON)) {
                screenOff = false;
                PreferencesHelper.setScreenOnTime(context);
                NotificationHelper.sendReturnNotification(context);
            } else if (action.equals("com.android.music.metachanged")) {

                String cmd = intent.getStringExtra("command");
                Log.v("tag ", action + " / " + cmd);
                String artist = intent.getStringExtra("artist");
                String album = intent.getStringExtra("album");
                String track = intent.getStringExtra("track");

                Song song = datasource.findSong(artist, track);
                if (song != null) {
                    int currentPlayCount = song.getPlayCount();
                    int newPlayCount = currentPlayCount + 1;
                    song.setPlayCount(newPlayCount);
                    datasource.updateSong(song);

                    if (newPlayCount > 10) {
                        NotificationHelper.sendMusicNotification(PhoneConscienceService.this);
                    }
                } else {
                    song = datasource.createSong(artist, track);
                }
                Log.v("tag", artist + ":" + album + ":" + track);
                Toast.makeText(PhoneConscienceService.this, song.toString(), Toast.LENGTH_SHORT).show();
            } else if (action.equals(STOP_PHONECONSCIENCE_SERVICE)) {
                stopForeground(true);
                stopSelf();
            }
        }
    }

    private void registerDetector() {
        mDropDetector = new DropDetector();
        mDropDetector.setListener(this);
        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        Sensor mSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mSensorManager.registerListener(mDropDetector, mSensor, SensorManager.SENSOR_DELAY_FASTEST);
    }

    private void unregisterDetector() {
        mSensorManager.unregisterListener(mDropDetector);
    }

    public void startDropDetection() {
        Toast.makeText(getApplicationContext(), "Starting Drop Detection", Toast.LENGTH_SHORT).show();
        registerDetector();
    }
}
