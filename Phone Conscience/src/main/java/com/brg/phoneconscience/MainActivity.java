package com.brg.phoneconscience;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.brg.phoneconscience.helpers.NotificationHelper;


public class MainActivity extends Activity {

    private static final String TAG = "PhoneConscience[MainActivity]";
    private static final String SERVICE_IS_RUNNING = "PhoneConscienceServiceIsRunning";

    private boolean mServiceIsRunning;
    private TextView mCallbackText; /** Some text view we are using to show state information. */
    private int numDrops;           /** Current number of drops detected. */
    boolean mIsBound;               /** Flag indicating whether we have called bind on the service. */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, "onCreate");
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey(SERVICE_IS_RUNNING)) {
                mServiceIsRunning = savedInstanceState.getBoolean(SERVICE_IS_RUNNING);
            }
        }

        if (mServiceIsRunning) {
            addMessenger();
        }

        Button startServiceBtn = (Button) findViewById(R.id.startServiceBtn);
        startServiceBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.this.startDropService();
//                MainActivity.this.bindDropService();
            }
        });

        Button stopServiceBtn = (Button) findViewById(R.id.stopServiceBtn);
        stopServiceBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                MainActivity.this.stopDropService();
                NotificationHelper.stopPersistentService(MainActivity.this);
//                MainActivity.this.unbindDropService();
            }
        });

        SharedPreferences sharedPreferences = getSharedPreferences("PhoneConscience", MODE_PRIVATE);
        numDrops = sharedPreferences.getInt("numDrops", 0);

        mCallbackText = (TextView)findViewById(R.id.numDropsLbl);
        mCallbackText.setText("" + numDrops);
    }

    @Override
    protected void onResume() {
        Log.i(TAG, "onResume");
        super.onResume();
        if (getIntent().hasExtra(PhoneConscienceService.RESET_NOTIFICATION)) {
            NotificationHelper.resetNotification(this);
        }
    }

    @Override
    protected void onPause() {
        Log.i(TAG, "onPause");
        if (mIsBound) {
            unbindDropService();
        }

        super.onPause();
    }

    @Override
    protected void onStop() {
        Log.i(TAG, "onStop");
        super.onStop();
    }

    protected void onDestroy() {
        Log.i(TAG, "onDestroy");
        removeMessenger();
        super.onDestroy();
    }

    protected void onRestart() {
        Log.i(TAG, "onRestart");
        super.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putBoolean(SERVICE_IS_RUNNING, mServiceIsRunning);
        super.onSaveInstanceState(outState);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            Intent settingsActivityIntent = new Intent(this, SettingsActivity.class);
            startActivity(settingsActivityIntent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /* Service */
    /** Messenger for communicating with service. */
    private Messenger mService;

    /**
     * Target we publish for clients to send messages to IncomingHandler.
     */
    final Messenger mMessenger = new Messenger(new IncomingHandler());

    private ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            Log.i(TAG, "onServiceConnected");
            mService = new Messenger(service);
            // log message that we're attached

            // We want to monitor the service for as long as we are
            // connected to it.
            try {
                Message msg = Message.obtain(null, PhoneConscienceService.MSG_REGISTER_CLIENT);
                msg.replyTo = mMessenger;
                mService.send(msg);
            } catch (RemoteException e) {
                // In this case the service has crashed before we could even
                // do anything with it; we can count on soon being
                // disconnected (and then reconnected if it can be restarted)
                // so there is no need to do anything here.
            }

            Toast.makeText(MainActivity.this, R.string.remote_service_connected,
                    Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mService = null;
            mCallbackText.setText("XXXX");

            try {
                Message msg = Message.obtain(null, PhoneConscienceService.MSG_UNREGISTER_CLIENT);
                msg.replyTo = mMessenger;
                mService.send(msg);
            } catch (RemoteException e) {
                Log.e(TAG, "onServiceDisconnected() --- Unable to send MSG_UNREGISTER_CLIENT to service");
            }

            // As part of the sample, tell the user what happened.
            Toast.makeText(MainActivity.this, R.string.remote_service_disconnected,
                    Toast.LENGTH_SHORT).show();
        }
    };

    private void startDropService() {
        if (!mServiceIsRunning) {
            mServiceIsRunning = true;
            Intent dropServiceIntent = new Intent(MainActivity.this, PhoneConscienceService.class);
            dropServiceIntent.putExtra(PhoneConscienceService.CLIENT, mMessenger);
            dropServiceIntent.putExtra(PhoneConscienceService.KEEP_CLIENT, true);
            startService(dropServiceIntent);
        }
    }

    private void stopDropService() {
        if (mServiceIsRunning) {
            mServiceIsRunning = false;
            Intent dropServiceIntent = new Intent(MainActivity.this, PhoneConscienceService.class);
            stopService(dropServiceIntent);
        }
    }

    private void addMessenger() {
        if (mServiceIsRunning) {
            Intent dropServiceIntent = new Intent(MainActivity.this, PhoneConscienceService.class);
            dropServiceIntent.putExtra(PhoneConscienceService.CLIENT, mMessenger);
            dropServiceIntent.putExtra(PhoneConscienceService.KEEP_CLIENT, true);
            startService(dropServiceIntent);
        }
    }

    private void removeMessenger() {
        if (mServiceIsRunning) {
            Intent dropServiceIntent = new Intent(MainActivity.this, PhoneConscienceService.class);
            dropServiceIntent.putExtra(PhoneConscienceService.CLIENT, mMessenger);
            dropServiceIntent.putExtra(PhoneConscienceService.KEEP_CLIENT, false);
            startService(dropServiceIntent);
        }
    }

    private void bindDropService() {
        Intent dropServiceIntent = new Intent(MainActivity.this, PhoneConscienceService.class);
        bindService(dropServiceIntent, mConnection,
                    Context.BIND_AUTO_CREATE + Context.BIND_DEBUG_UNBIND);
        mIsBound = true;
    }

    private void unbindDropService() {
        if (mIsBound) {
            // If we have received the service, and hence registered with
            // it, then now is the time to unregister.
            if (mService != null) {
                try {
                    Message msg = Message.obtain(null, PhoneConscienceService.MSG_UNREGISTER_CLIENT);
                    msg.replyTo = mMessenger;
                    mService.send(msg);
                } catch (RemoteException e) {
                    // There is nothing special we need to do if the service
                    // has crashed.
                    Log.e(TAG, "unbindDropService() --- Unable to send MSG_UNREGISTER_CLIENT to service");
                }
            }

            // Detach our existing connection.
            unbindService(mConnection);
            mIsBound = false;
        }
    }

    /**
     * Handler of incoming messages from service.
     */
    class IncomingHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case PhoneConscienceService.MSG_DROP_DETECTED:
                    SharedPreferences sharedPreferences = getSharedPreferences("PhoneConscience", MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putInt("numDrops", ++numDrops);
                    editor.commit();

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mCallbackText.setText("" + numDrops);
                        }
                    });

//                    sendDropNotification(this);
                    break;
                default:
                    super.handleMessage(msg);
            }
        }
    }
}
