package com.brg.phoneconscience;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.SeekBar;
import android.widget.TextView;

public class SettingsActivity extends Activity implements SeekBar.OnSeekBarChangeListener {

    private SeekBar freeFallSeekBar;
    private SeekBar suddenStopSeekBar;

    private TextView freeFallTextView;
    private TextView suddenStopTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_settings);

        freeFallSeekBar = (SeekBar)findViewById(R.id.freeFallSeekBar);
        freeFallSeekBar.setOnSeekBarChangeListener(this);
        suddenStopSeekBar = (SeekBar)findViewById(R.id.suddenStopSeekBar);
        suddenStopSeekBar.setOnSeekBarChangeListener(this);

        freeFallTextView = (TextView)findViewById(R.id.freeFallTextView);
        suddenStopTextView = (TextView)findViewById(R.id.suddenStopTextView);

        SharedPreferences sharedPreferences = getSharedPreferences("PhoneConscience", MODE_PRIVATE);
        float freeFallValue = sharedPreferences.getFloat(DropDetector.FREE_FALL_SENSITIVITY, 1.75f);
        float suddenStopValue = sharedPreferences.getFloat(DropDetector.SUDDEN_STOP_SENSITIVITY, 30.0f);
        freeFallTextView.setText(Float.toString(freeFallValue));
        freeFallSeekBar.setProgress((int)(freeFallValue * 100));
        suddenStopTextView.setText(Float.toString(suddenStopValue));
        suddenStopSeekBar.setProgress((int)(suddenStopValue * 100));
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        if (seekBar.getId() == R.id.freeFallSeekBar) {
            freeFallTextView.setText(Float.toString(progress/100));
        } else if (seekBar.getId() == R.id.suddenStopSeekBar) {
            suddenStopTextView.setText(Float.toString(progress/100));
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        if (seekBar.getId() == R.id.freeFallSeekBar) {
            SharedPreferences sharedPreferences = getSharedPreferences("PhoneConscience", MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putFloat(DropDetector.FREE_FALL_SENSITIVITY, seekBar.getProgress()/100);
            editor.commit();
        } else if (seekBar.getId() == R.id.suddenStopSeekBar) {
            SharedPreferences sharedPreferences = getSharedPreferences("PhoneConscience", MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putFloat(DropDetector.SUDDEN_STOP_SENSITIVITY, seekBar.getProgress()/100);
            editor.commit();
        }
    }
}
