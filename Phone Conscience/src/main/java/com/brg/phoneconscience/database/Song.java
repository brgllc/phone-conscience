package com.brg.phoneconscience.database;

/**
 * Created by jmo on 5/21/2014.
 */
public class Song {
    private long    id;
    private String  artist;
    private String  track;
    private int     playCount;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getTrack() {
        return track;
    }

    public void setTrack(String track) {
        this.track = track;
    }

    public int getPlayCount() {
        return playCount;
    }

    public void setPlayCount(int playCount) {
        this.playCount = playCount;
    }

    @Override
    public String toString() {
        StringBuilder tbr = new StringBuilder();
        tbr.append(artist).append(" - ").append(track).append(" (").append(playCount).append(")");
        return tbr.toString();
    }
}
