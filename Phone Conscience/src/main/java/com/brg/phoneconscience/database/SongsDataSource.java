package com.brg.phoneconscience.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jmo on 5/21/2014.
 */
public class SongsDataSource {
    // Database fields
    private SQLiteDatabase database;
    private SongTrackerDatabaseHelper dbHelper;
    private String[] allColumns = {
            SongTrackerDatabaseHelper.COLUMN_ID,
            SongTrackerDatabaseHelper.COLUMN_ARTIST,
            SongTrackerDatabaseHelper.COLUMN_TRACK,
            SongTrackerDatabaseHelper.COLUMN_PLAY_COUNT};

    public SongsDataSource(Context context) {
        dbHelper = new SongTrackerDatabaseHelper(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public Song createSong(String artist, String track) {
        ContentValues values = new ContentValues();
        values.put(SongTrackerDatabaseHelper.COLUMN_ARTIST, artist);
        values.put(SongTrackerDatabaseHelper.COLUMN_TRACK, track);
        values.put(SongTrackerDatabaseHelper.COLUMN_PLAY_COUNT, 1);
        long insertId = database.insert(SongTrackerDatabaseHelper.SONG_TRACKER_TABLE_NAME, null, values);
        Cursor cursor = database.query(SongTrackerDatabaseHelper.SONG_TRACKER_TABLE_NAME,
                allColumns, SongTrackerDatabaseHelper.COLUMN_ID + " = " + insertId, null,
                null, null, null);
        cursor.moveToFirst();
        Song newSong = cursorToSong(cursor);
        cursor.close();
        return newSong;
    }

    public void updateSong(Song song) {
        long id = song.getId();
        System.out.println("Song updated with play count: " + song.getPlayCount());

        ContentValues values = new ContentValues();
        values.put(SongTrackerDatabaseHelper.COLUMN_PLAY_COUNT, song.getPlayCount());

        database.update(SongTrackerDatabaseHelper.SONG_TRACKER_TABLE_NAME, values,
                        SongTrackerDatabaseHelper.COLUMN_ID + " = " + id, null);
    }

    public Song findSong(String artist, String track) {
        Song tbr = null;
        String whereClause = new StringBuilder().append(SongTrackerDatabaseHelper.COLUMN_ARTIST)
                                                .append(" = ?")
                                                .append(" AND ")
                                                .append(SongTrackerDatabaseHelper.COLUMN_TRACK)
                                                .append(" = ?")
                                                .toString();
        String[] whereArgs = new String[]{artist, track};
        Cursor cursor = database.query(SongTrackerDatabaseHelper.SONG_TRACKER_TABLE_NAME,
                                        allColumns, whereClause, whereArgs, null, null, null);
        if (cursor.moveToFirst()) {
            tbr = cursorToSong(cursor);
        }
        cursor.close();
        return tbr;
    }

    public void deleteSong(Song song) {
        long id = song.getId();
        System.out.println("Song deleted with id: " + id);
        database.delete(SongTrackerDatabaseHelper.SONG_TRACKER_TABLE_NAME,
                        SongTrackerDatabaseHelper.COLUMN_ID + " = " + id, null);
    }

    public List<Song> getAllSongs() {
        List<Song> songs = new ArrayList<Song>();

        Cursor cursor = database.query(SongTrackerDatabaseHelper.SONG_TRACKER_TABLE_NAME,
                allColumns, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Song song = cursorToSong(cursor);
            songs.add(song);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return songs;
    }

    private Song cursorToSong(Cursor cursor) {
        Song song = new Song();
        song.setId(cursor.getLong(0));
        song.setArtist(cursor.getString(1));
        song.setTrack(cursor.getString(2));
        song.setPlayCount(cursor.getInt(3));
        return song;
    }
}
