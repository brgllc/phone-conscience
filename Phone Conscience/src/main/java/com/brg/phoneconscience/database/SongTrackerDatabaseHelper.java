package com.brg.phoneconscience.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by jmo on 5/21/2014.
 */
public class SongTrackerDatabaseHelper extends SQLiteOpenHelper {

    private static final int    DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "songTracker.db";

    public  static final String COLUMN_ID           = "_id";
    public  static final String COLUMN_ARTIST       = "ARTIST";
    public  static final String COLUMN_TRACK        = "TRACK";
    public  static final String COLUMN_PLAY_COUNT   = "PLAY_COUNT";

    public  static final String SONG_TRACKER_TABLE_NAME = "songTracker";
    private static final String SONG_TRACKER_TABLE_CREATE =
            "CREATE TABLE " + SONG_TRACKER_TABLE_NAME + " (" +
                    COLUMN_ID + " integer primary key autoincrement, " +
                    COLUMN_ARTIST + " TEXT, " +
                    COLUMN_TRACK + " TEXT, " +
                    COLUMN_PLAY_COUNT + " integer);";

    SongTrackerDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SONG_TRACKER_TABLE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        // TODO Not be a dick and implement data migration
        Log.w(SongTrackerDatabaseHelper.class.getName(),
                "Upgrading database from version " + oldVersion + " to "
                        + newVersion + ", which will destroy all old data"
        );
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + SONG_TRACKER_TABLE_NAME);
        onCreate(sqLiteDatabase);
    }
}
