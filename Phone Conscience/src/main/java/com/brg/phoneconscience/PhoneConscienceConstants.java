package com.brg.phoneconscience;

/**
 * Created by jmo on 6/1/2014.
 */
public class PhoneConscienceConstants {

    public static String SCREEN_ON_TIME     = "screenOnTime";
    public static String SCREEN_OFF_TIME    = "screenOffTime";

    public static long AWAY_TIME_INTERVAL   = 1800000; // 30 minutes
}
