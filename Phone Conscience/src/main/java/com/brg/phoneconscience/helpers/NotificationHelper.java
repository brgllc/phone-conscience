package com.brg.phoneconscience.helpers;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;

import com.brg.phoneconscience.MainActivity;
import com.brg.phoneconscience.PhoneConscienceConstants;
import com.brg.phoneconscience.PhoneConscienceService;
import com.brg.phoneconscience.R;

/**
 * Created by jmo on 5/26/2014.
 */
public class NotificationHelper {

    public static void startPersistent(Service service) {
        Intent i = new Intent(service, MainActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        i.putExtra(PhoneConscienceService.RESET_NOTIFICATION, true);
        PendingIntent resultPendingIntent = PendingIntent.getActivity(service, 0, i, 0);
        Notification notice = new NotificationCompat.Builder(service)
                .setSmallIcon(R.drawable.ic_happy_icon)
                .setContentText(service.getString(R.string.default_standby))
                .setContentTitle(service.getString(R.string.app_name))
                .setOngoing(true)
                .setContentIntent(resultPendingIntent)
                .build();

        notice.flags |= Notification.FLAG_NO_CLEAR;
        service.startForeground(R.drawable.ic_launcher, notice);
    }

    /**
     * Show a notification while this service is running.
     */
    public static void showNotification(Context context) {
        // In this sample, we'll use the same text for the ticker and the expanded notification
        CharSequence text = context.getText(R.string.remote_service_started);

        // Set the icon, scrolling text and timestamp
//        Notification notification = new Notification(R.drawable.stat_sample, text,
//                System.currentTimeMillis());

        // The PendingIntent to launch our activity if the user selects this notification
        PendingIntent contentIntent = PendingIntent.getActivity(context, 0,
                new Intent(context, MainActivity.class), 0);

        Notification notification = new NotificationCompat.Builder(context)
                .setContentTitle(context.getText(R.string.service_name))
                .setContentText(context.getText(R.string.remote_service_created))
                .setSmallIcon(R.drawable.ic_happy_icon)
                .setContentIntent(contentIntent)
                .setAutoCancel(false)
                .build();

        // Set the info for the views that show in the notification panel.
//        notification.setLatestEventInfo(this, getText(R.string.remote_service_label),
//                text, contentIntent);

        // Send the notification.
        // We use a string id because it is a unique number.  We use it later to cancel.
        NotificationManager mNM = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        mNM.notify(R.drawable.ic_launcher, notification);
    }

    public static void sendDropNotification(Context context) {
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context)
                        .setSmallIcon(R.drawable.ic_angry_icon)
                        .setContentTitle(context.getString(R.string.drop_content_title))
                        .setContentText(context.getString(R.string.drop_content_description_1));

        // Creates an explicit intent for an Activity in your app
        Intent resultIntent = new Intent(context, MainActivity.class);
        resultIntent.putExtra(PhoneConscienceService.RESET_NOTIFICATION, true);

        // The stack builder object will contain an artificial back stack for the
        // started Activity.
        // This ensures that navigating backward from the Activity leads out of
        // your application to the Home screen.
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        // Adds the back stack for the Intent (but not the Intent itself)
        stackBuilder.addParentStack(MainActivity.class);
        // Adds the Intent that starts the Activity to the top of the stack
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(resultPendingIntent);

        // mId allows you to update the notification later on.
        NotificationManager mNM = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        mNM.notify(R.drawable.ic_launcher, mBuilder.build());
    }

    public static void cancelNotifications(Context context) {
        NotificationManager mNM = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
//        mNM.cancel(R.drawable.ic_stat_phone_conscience_icon);
//        mNM.cancel(R.string.remote_service_created);
        mNM.cancelAll();
    }

    public static void sendMusicNotification(Context context) {

    }

    public static void sendReturnNotification(Context context) {
        long offTime = PreferencesHelper.getScreenOffTime(context);
        long onTime = PreferencesHelper.getScreenOnTime(context);

        if (onTime - offTime >= PhoneConscienceConstants.AWAY_TIME_INTERVAL) {
            // update persistent notification with return message
        }
    }

    public static void stopPersistentService(Context context) {
        Intent stopPhoneconscienceServiceIntent = new Intent(context, PhoneConscienceService.class);
        context.stopService(stopPhoneconscienceServiceIntent);
    }

    public static void resetNotification(Context context) {
        Intent i = new Intent(context, MainActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        i.putExtra(PhoneConscienceService.RESET_NOTIFICATION, true);
        PendingIntent resultPendingIntent = PendingIntent.getActivity(context, 0, i, 0);
        Notification notice = new NotificationCompat.Builder(context)
                .setSmallIcon(R.drawable.ic_happy_icon)
                .setContentText(context.getString(R.string.default_standby))
                .setContentTitle(context.getString(R.string.app_name))
                .setOngoing(true)
                .setContentIntent(resultPendingIntent)
                .build();

        notice.flags |= Notification.FLAG_NO_CLEAR;
        NotificationManager mNM = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        mNM.notify(R.drawable.ic_launcher, notice);
    }
}
