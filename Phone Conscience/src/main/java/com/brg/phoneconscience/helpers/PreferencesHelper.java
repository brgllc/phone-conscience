package com.brg.phoneconscience.helpers;

import android.content.Context;
import android.content.SharedPreferences;

import com.brg.phoneconscience.PhoneConscienceConstants;

/**
 * Created by jmo on 6/1/2014.
 */
public class PreferencesHelper {

    public static void setScreenOffTime(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("PhoneConscience", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putLong(PhoneConscienceConstants.SCREEN_OFF_TIME, System.currentTimeMillis());
        editor.commit();
    }

    public static void setScreenOnTime(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("PhoneConscience", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putLong(PhoneConscienceConstants.SCREEN_ON_TIME, System.currentTimeMillis());
        editor.commit();
    }

    public static long getScreenOffTime(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("PhoneConscience", Context.MODE_PRIVATE);
        return sharedPreferences.getLong(PhoneConscienceConstants.SCREEN_OFF_TIME, 0);
    }

    public static long getScreenOnTime(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("PhoneConscience", Context.MODE_PRIVATE);
        return sharedPreferences.getLong(PhoneConscienceConstants.SCREEN_ON_TIME, 0);
    }
}
