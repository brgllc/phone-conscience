package com.brg.phoneconscience;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

/**
 * Created by jmoralez on 4/21/14.
 */
public class DropDetector implements SensorEventListener {

    private boolean freeFallDetected    = false;
    private boolean suddenStopDetected  = false;

    public static String FREE_FALL_SENSITIVITY = "freeFallSensitivity";
    public static String SUDDEN_STOP_SENSITIVITY = "suddenStopSensitivity";

    private DropDetectorListener mListener;

    public interface DropDetectorListener {
        public void dropDetected();
        public SharedPreferences getSharedPreferences();
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        Sensor sensor = event.sensor;
        synchronized (this) {
            if (sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
                double gravity = SensorManager.STANDARD_GRAVITY;
                double xx   = event.values[0];
                double yy   = event.values[1];
                double zz   = event.values[2];
                double aaa  = Math.round(Math.sqrt(Math.pow(xx, 2) + Math.pow(yy, 2) + Math.pow(zz, 2)));

                SharedPreferences sharedPreferences = mListener.getSharedPreferences();
                float freeFallSensitivty = sharedPreferences.getFloat(FREE_FALL_SENSITIVITY, 1.75f);
                float suddenStopSensitivty = sharedPreferences.getFloat(FREE_FALL_SENSITIVITY, 30.0f);
                if (aaa <= freeFallSensitivty) {
                    freeFallDetected = true;
                    //mintime=System.currentTimeMillis();
                } else if (aaa >= suddenStopSensitivty) {
                    suddenStopDetected = true;
                } else if (!freeFallDetected) {
                    freeFallDetected = false;
                    suddenStopDetected = false;
                }

                if (freeFallDetected && suddenStopDetected) {
                    freeFallDetected = false;
                    suddenStopDetected = false;

                    if (mListener != null) {
                        mListener.dropDetected();
                    }
                }
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    public void setListener(DropDetectorListener listener) {
        this.mListener = listener;
    }

    public DropDetectorListener getListener() {
        return this.mListener;
    }
}
